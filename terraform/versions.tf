terraform {
  required_version = "~>1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 4.0"
    }
  }

  backend "s3" {
    bucket = "terraform-state-faaizz-idp"
    key    = "ec2-instance-scheduler.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  default_tags {
    tags = {
      Project = "PodFebruary"
      Class   = "Pod"
    }
  }
}
