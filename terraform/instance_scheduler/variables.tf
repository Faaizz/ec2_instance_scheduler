variable "stack_name" {
  description = "Stack name."
  type        = string
  default     = "instance-scheduler"
  validation {
    condition     = length(var.stack_name) > 1 && length(var.stack_name) < 127
    error_message = "Please specify a stack name between 1 and 127 characters long."
  }
}
variable "scheduling_active" {
  description = "Activate or deactivate scheduling."
  type        = string
  default     = "Yes"
  validation {
    condition     = contains(["Yes", "No"], var.scheduling_active)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
variable "scheduled_services" {
  description = "Scheduled Services."
  type        = string
  default     = "EC2"
  validation {
    condition     = contains(["EC2", "RDS", "Both"], var.scheduled_services)
    error_message = "Value can only be 'EC2', 'RDS' or 'Both'"
  }
}
variable "schedule_rds_clusters" {
  description = "Enable scheduling of Aurora clusters for RDS Service."
  type        = string
  default     = "No"
  validation {
    condition     = contains(["Yes", "No"], var.schedule_rds_clusters)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
variable "create_rds_snapshot" {
  description = "Create snapshot before stopping RDS instances (does not apply to Aurora Clusters)."
  type        = string
  default     = "No"
  validation {
    condition     = contains(["Yes", "No"], var.create_rds_snapshot)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
variable "memory_size" {
  description = "Size of the Lambda function running the scheduler, increase size when processing large numbers of instances."
  type        = number
  default     = 128
  validation {
    condition = contains([
      128, 384, 512, 640, 768, 896, 1024, 1152, 1280, 1408, 1536,
    ], var.memory_size)
    error_message = "Specify one of: 128, 384, 512, 640, 768, 896, 1024, 1152, 1280, 1408, or 1536"
  }
}
variable "use_cloud_watch_metrics" {
  description = "Collect instance scheduling data using CloudWatch metrics."
  type        = string
  default     = "No"
  validation {
    condition     = contains(["Yes", "No"], var.use_cloud_watch_metrics)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
variable "log_retention_days" {
  description = "Retention days for scheduler logs."
  type        = number
  default     = 30
  validation {
    condition = contains([
      1, 3, 5, 7, 14, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653,
    ], var.log_retention_days)
    error_message = "Specify one of: 1, 3, 5, 7, 14, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, or 3653"
  }
}
variable "trace" {
  description = "Enable logging of detailed information in CloudWatch logs."
  type        = string
  default     = "No"
  validation {
    condition     = contains(["Yes", "No"], var.trace)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
variable "enable_ssm_maintenance_windows" {
  description = "Enable the solution to load SSM Maintenance Windows, so that they can be used for EC2 instance Scheduling."
  type        = string
  default     = "No"
  validation {
    condition     = contains(["Yes", "No"], var.enable_ssm_maintenance_windows)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
variable "tag_name" {
  description = "Name of tag to use for associating instance schedule schemas with service instances."
  type        = string
  default     = "Schedule"
  validation {
    condition     = length(var.tag_name) > 1 && length(var.tag_name) < 127
    error_message = "Please specify a tag name between 1 and 127 characters long."
  }
}
variable "default_timezone" {
  description = "Choose the default Time Zone. Default is 'UTC'."
  type        = string
  default     = "UTC"
  validation {
    condition = contains([
      "Africa/Abidjan",
      "Africa/Accra",
      "Africa/Cairo",
      "Africa/Johannesburg",
      "Africa/Lagos",
      "America/New_York",
      "America/Toronto",
      "Europe/Amsterdam",
      "Europe/Berlin",
      "Europe/Dublin",
      "Europe/Istanbul",
      "Europe/London",
      "Europe/Madrid",
      "Europe/Moscow",
      "Europe/Paris",
      "Europe/Rome",
      "Europe/Zurich",
      "GMT",
      "US/Alaska",
      "US/Arizona",
      "US/Central",
      "US/Eastern",
      "US/Hawaii",
      "US/Mountain",
      "US/Pacific",
      "UTC",
    ], var.default_timezone)
    error_message = "Please specify an allowed time zone."
  }
}
variable "regions" {
  description = "List of regions in which instances are scheduled, leave blank for current region only."
  type        = list(string)
  default     = ["eu-central-1"]
}
variable "cross_account_roles" {
  description = "Comma separated list of ARN's for cross account access roles. These roles must be created in all checked accounts the scheduler to start and stop instances."
  type        = list(string)
  default     = []
}
variable "started_tags" {
  description = "Comma separated list of tagname and values on the formt name=value,name=value,.. that are set on started instances."
  type        = string
  default     = ""
}
variable "stopped_tags" {
  description = "Comma separated list of tagname and values on the formt name=value,name=value,.. that are set on stopped instances."
  type        = string
  default     = ""
}
variable "scheduler_frequency" {
  description = "Scheduler running frequency in minutes."
  type        = number
  default     = 5
  validation {
    condition     = contains([1, 2, 5, 10, 15, 30, 60], var.scheduler_frequency)
    error_message = "Please specify one of: 1, 2, 5, 10, 15, 30, or 60"
  }
}
variable "schedule_lambda_account" {
  description = "Schedule instances in this account."
  type        = string
  default     = "Yes"
  validation {
    condition     = contains(["Yes", "No"], var.schedule_lambda_account)
    error_message = "Value can only be 'Yes' or 'No'"
  }
}
