# instance_scheduler

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.55.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.55.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudformation_stack.main](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/cloudformation_stack) | resource |
| [aws_sns_topic.stack](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_rds_snapshot"></a> [create\_rds\_snapshot](#input\_create\_rds\_snapshot) | Create snapshot before stopping RDS instances (does not apply to Aurora Clusters). | `string` | `"No"` | no |
| <a name="input_cross_account_roles"></a> [cross\_account\_roles](#input\_cross\_account\_roles) | Comma separated list of ARN's for cross account access roles. These roles must be created in all checked accounts the scheduler to start and stop instances. | `list(string)` | `[]` | no |
| <a name="input_default_timezone"></a> [default\_timezone](#input\_default\_timezone) | Choose the default Time Zone. Default is 'UTC'. | `string` | `"UTC"` | no |
| <a name="input_enable_ssm_maintenance_windows"></a> [enable\_ssm\_maintenance\_windows](#input\_enable\_ssm\_maintenance\_windows) | Enable the solution to load SSM Maintenance Windows, so that they can be used for EC2 instance Scheduling. | `string` | `"No"` | no |
| <a name="input_log_retention_days"></a> [log\_retention\_days](#input\_log\_retention\_days) | Retention days for scheduler logs. | `number` | `30` | no |
| <a name="input_memory_size"></a> [memory\_size](#input\_memory\_size) | Size of the Lambda function running the scheduler, increase size when processing large numbers of instances. | `number` | `128` | no |
| <a name="input_regions"></a> [regions](#input\_regions) | List of regions in which instances are scheduled, leave blank for current region only. | `list(string)` | <pre>[<br>  "eu-central-1"<br>]</pre> | no |
| <a name="input_schedule_lambda_account"></a> [schedule\_lambda\_account](#input\_schedule\_lambda\_account) | Schedule instances in this account. | `string` | `"Yes"` | no |
| <a name="input_schedule_rds_clusters"></a> [schedule\_rds\_clusters](#input\_schedule\_rds\_clusters) | Enable scheduling of Aurora clusters for RDS Service. | `string` | `"No"` | no |
| <a name="input_scheduled_services"></a> [scheduled\_services](#input\_scheduled\_services) | Scheduled Services. | `string` | `"EC2"` | no |
| <a name="input_scheduler_frequency"></a> [scheduler\_frequency](#input\_scheduler\_frequency) | Scheduler running frequency in minutes. | `number` | `5` | no |
| <a name="input_scheduling_active"></a> [scheduling\_active](#input\_scheduling\_active) | Activate or deactivate scheduling. | `string` | `"Yes"` | no |
| <a name="input_stack_name"></a> [stack\_name](#input\_stack\_name) | Stack name. | `string` | `"instance-scheduler"` | no |
| <a name="input_started_tags"></a> [started\_tags](#input\_started\_tags) | Comma separated list of tagname and values on the formt name=value,name=value,.. that are set on started instances. | `string` | `""` | no |
| <a name="input_stopped_tags"></a> [stopped\_tags](#input\_stopped\_tags) | Comma separated list of tagname and values on the formt name=value,name=value,.. that are set on stopped instances. | `string` | `""` | no |
| <a name="input_tag_name"></a> [tag\_name](#input\_tag\_name) | Name of tag to use for associating instance schedule schemas with service instances. | `string` | `"Schedule"` | no |
| <a name="input_trace"></a> [trace](#input\_trace) | Enable logging of detailed information in CloudWatch logs. | `string` | `"No"` | no |
| <a name="input_use_cloud_watch_metrics"></a> [use\_cloud\_watch\_metrics](#input\_use\_cloud\_watch\_metrics) | Collect instance scheduling data using CloudWatch metrics. | `string` | `"No"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_configuration_table_arn"></a> [configuration\_table\_arn](#output\_configuration\_table\_arn) | ARN of EC2 Instance Scheduler DynamoDB configuration table |
| <a name="output_sns_topic_application"></a> [sns\_topic\_application](#output\_sns\_topic\_application) | Topic to subscribe to for notifications of errors and warnings |
| <a name="output_sns_topic_stack_creation"></a> [sns\_topic\_stack\_creation](#output\_sns\_topic\_stack\_creation) | Topic to subscribe to for notifications of errors and warnings during stack creation |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
