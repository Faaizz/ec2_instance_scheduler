# CloudFormation
resource "aws_cloudformation_stack" "main" {
  name = var.stack_name

  template_url = local.template.url

  parameters = {
    SchedulingActive            = var.scheduling_active
    ScheduledServices           = var.scheduled_services
    ScheduleRdsClusters         = var.schedule_rds_clusters
    CreateRdsSnapshot           = var.create_rds_snapshot
    MemorySize                  = tostring(var.memory_size)
    UseCloudWatchMetrics        = var.use_cloud_watch_metrics
    LogRetentionDays            = tostring(var.log_retention_days)
    Trace                       = var.trace
    EnableSSMMaintenanceWindows = var.enable_ssm_maintenance_windows
    TagName                     = var.tag_name
    DefaultTimezone             = var.default_timezone
    Regions                     = join(", ", var.regions)
    CrossAccountRoles           = join(", ", var.cross_account_roles)
    StartedTags                 = var.started_tags
    StoppedTags                 = var.stopped_tags
    SchedulerFrequency          = tostring(var.scheduler_frequency)
    ScheduleLambdaAccount       = var.schedule_lambda_account
  }

  capabilities      = ["CAPABILITY_IAM"]
  notification_arns = [aws_sns_topic.stack.arn]

}

resource "aws_sns_topic" "stack" {
  name_prefix       = "${var.stack_name}-stack-updates"
  kms_master_key_id = "alias/aws/sns"
}
