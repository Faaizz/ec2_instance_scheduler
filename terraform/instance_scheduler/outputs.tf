output "configuration_table_arn" {
  description = "ARN of EC2 Instance Scheduler DynamoDB configuration table"
  value       = aws_cloudformation_stack.main.outputs["ConfigurationTable"]
}

output "sns_topic_stack_creation" {
  description = "Topic to subscribe to for notifications of errors and warnings during stack creation"
  value       = aws_sns_topic.stack.arn
}

output "sns_topic_application" {
  description = "Topic to subscribe to for notifications of errors and warnings"
  value       = aws_cloudformation_stack.main.outputs["IssueSnsTopicArn"]
}
