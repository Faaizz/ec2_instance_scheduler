module "instance_scheduler" {
  source = "./instance_scheduler"

  stack_name = local.stack_name
}

module "default_schedules" {
  source = "./schedule"

  for_each = local.default_schedules

  configuration_table = split("/", module.instance_scheduler.configuration_table_arn)[1]

  name                   = each.key
  description            = each.value.description
  timezone               = each.value.timezone
  hibernate              = can(each.value.hibernate) ? each.value.hibernate : false
  enforced               = can(each.value.enforced) ? each.value.enforced : false
  use_maintenance_window = can(each.value.use_maintenance_window) ? each.value.use_maintenance_window : false
  ssm_maintenance_window = can(each.value.ssm_maintenance_window) ? each.value.ssm_maintenance_window : ""
  use_metrics            = can(each.value.use_metrics) ? each.value.use_metrics : false
  override_status        = can(each.value.override_status) ? each.value.override_status : ""
  retain_running         = can(each.value.retain_running) ? each.value.retain_running : false
  stop_new_instances     = can(each.value.stop_new_instances) ? each.value.stop_new_instances : false

  periods = each.value.periods
}

module "additional_schedules" {
  source = "./schedule"

  for_each = { for as in var.additional_schedules : as.name => as }

  configuration_table = split("/", module.instance_scheduler.configuration_table_arn)[1]

  name                   = each.key
  description            = each.value.description
  timezone               = each.value.timezone
  hibernate              = can(each.value.hibernate) ? each.value.hibernate : false
  enforced               = can(each.value.enforced) ? each.value.enforced : false
  use_maintenance_window = can(each.value.use_maintenance_window) ? each.value.use_maintenance_window : false
  ssm_maintenance_window = can(each.value.ssm_maintenance_window) ? each.value.ssm_maintenance_window : ""
  use_metrics            = can(each.value.use_metrics) ? each.value.use_metrics : false
  override_status        = can(each.value.override_status) ? each.value.override_status : ""
  retain_running         = can(each.value.retain_running) ? each.value.retain_running : false
  stop_new_instances     = can(each.value.stop_new_instances) ? each.value.stop_new_instances : false

  periods = each.value.periods
}

module "notification_filter" {
  source = "./notification_filter"

  stack_name = local.stack_name
  source_arn = module.instance_scheduler.sns_topic_stack_creation
}

resource "aws_sns_topic_subscription" "stack_creation" {
  for_each = { for idx, sub in var.sns_subscriptions : idx => sub }

  topic_arn = module.notification_filter.sns_topic_arn
  protocol  = each.value.protocol
  endpoint  = each.value.endpoint
}

resource "aws_sns_topic_subscription" "stack_execution" {
  for_each = { for idx, sub in var.sns_subscriptions : idx => sub }

  topic_arn = module.instance_scheduler.sns_topic_application
  protocol  = each.value.protocol
  endpoint  = each.value.endpoint
}
