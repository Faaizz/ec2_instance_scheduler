data "archive_file" "lambda_zip_inline" {
  type        = "zip"
  output_path = local.output_file_path
  source {
    filename = "main.js"
    content  = <<-EOF
    var topic_arn = process.env.DEST_TOPIC_ARN;
    var stack_name = process.env.STACK_NAME;
    var AWS = require('aws-sdk'); 
    AWS.config.region_array = topic_arn.split(':'); // splits the ARN into an array 
    AWS.config.region = AWS.config.region_array[3];  // makes the 4th variable in the array (will always be the region)

    exports.handler = function(event, context) {
      const message = event.Records[0].Sns.Message;
      var fields = message.split("\n");
      var resource_name = fields[3].replace(/LogicalResourceId=/g, '');
      var resource_type = fields[9].replace(/ResourceType=/g, '');
      var subject = resource_type + " " + resource_name;

      console.log('SUBJECT: ' + subject);
      check_status(subject, "CREATE_COMPLETE", message);
      check_status(subject, "CREATE_FAILED", message);
      check_status(subject, "ROLLBACK_IN_PROGRESS", message);
      check_status(subject, "ROLLBACK_FAILED", message);
      check_status(subject, "UPDATE_FAILED", message);
      check_status(subject, "UPDATE_ROLLBACK_IN_PROGRESS", message);
      check_status(subject, "UPDATE_ROLLBACK_FAILED", message);
    };

    function check_status(subject, status, message) {
      if (message.indexOf(status) > -1) {
        console.log('sending notification for: ' + subject);
        send_SNS_notification(subject, status, message); 
      }
    }

    function send_SNS_notification(subject, status, message) {
      var sns = new AWS.SNS();
      subject = subject + " is in " + status;
      sns.publish({ 
        Subject: subject,
        Message: message,
        TopicArn: topic_arn
      }, function(err, data) {
          if (err) {
            console.log(err.stack);
            return;
          } 
          console.log('push sent');
          console.log(data);
      });
    }
    EOF
  }
}
