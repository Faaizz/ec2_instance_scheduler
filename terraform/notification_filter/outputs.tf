output "sns_topic_arn" {
  description = "SNS topic for filtered stack creation notifications."
  value       = aws_sns_topic.filtered.arn
}
