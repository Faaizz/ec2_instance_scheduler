# notification_filter

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | 2.3.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.55.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.3.0 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.55.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.filter](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/iam_role) | resource |
| [aws_lambda_function.filter](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/lambda_function) | resource |
| [aws_lambda_permission.allow_sns](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/lambda_permission) | resource |
| [aws_sns_topic.filtered](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic) | resource |
| [aws_sns_topic_subscription.filter](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic_subscription) | resource |
| [archive_file.lambda_zip_inline](https://registry.terraform.io/providers/hashicorp/archive/2.3.0/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_source_arn"></a> [source\_arn](#input\_source\_arn) | ARN of SNS topic. | `string` | n/a | yes |
| <a name="input_stack_name"></a> [stack\_name](#input\_stack\_name) | Stack name. | `string` | `"instance-scheduler"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_sns_topic_arn"></a> [sns\_topic\_arn](#output\_sns\_topic\_arn) | SNS topic for filtered stack creation notifications. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
