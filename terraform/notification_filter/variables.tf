variable "stack_name" {
  description = "Stack name."
  type        = string
  default     = "instance-scheduler"
  validation {
    condition     = length(var.stack_name) > 1 && length(var.stack_name) < 127
    error_message = "Please specify a stack name between 1 and 127 characters long."
  }
}

variable "source_arn" {
  description = "ARN of SNS topic."
  type        = string
  validation {
    condition = can(regex(
      "^arn:aws:sns:[A-Za-z-_0-9]+:[0-9]{12}:[A-Za-z-_0-9]+",
      var.source_arn,
    ))
    error_message = "Please provide a valid SNS topic ARN. E.g., `arn:aws:sns:REGION:012345678910:TOPIC_NAME`."
  }
}
