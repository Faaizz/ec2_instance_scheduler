resource "aws_sns_topic" "filtered" {
  name_prefix       = "${var.stack_name}-filtered-stack-updates"
  kms_master_key_id = "alias/aws/sns"
}

resource "aws_iam_role" "filter" {
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "my_inline_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action = [
            "sns:Publish",
          ]
          Effect = "Allow"
          Resource = [
            aws_sns_topic.filtered.arn,
          ]
        },
        {
          Action = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
          ]
          Effect = "Allow"
          Resource = [
            "arn:aws:logs:*:*:*",
          ]
        },
      ]
    })
  }

}

resource "aws_lambda_function" "filter" {
  function_name = "${var.stack_name}-filter-stack-updates"
  filename      = local.output_file_path
  role          = aws_iam_role.filter.arn
  handler       = "main.handler"
  runtime       = "nodejs16.x"
  timeout       = 180

  environment {
    variables = {
      DEST_TOPIC_ARN = aws_sns_topic.filtered.arn
      STACK_NAME     = var.stack_name
    }
  }

  depends_on = [
    data.archive_file.lambda_zip_inline,
  ]
}

resource "aws_lambda_permission" "allow_sns" {
  statement_id  = "AllowExecutionFromStackNotifications"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.filter.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = var.source_arn
}

resource "aws_sns_topic_subscription" "filter" {
  topic_arn = var.source_arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.filter.arn
}
