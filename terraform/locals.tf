locals {
  stack_name = "instance-scheduler"

  default_schedules = {
    "office-hours-de" = {
      description            = "Office hours Germany"
      timezone               = "Europe/Berlin"
      hibernate              = false
      enforced               = false
      use_maintenance_window = false
      ssm_maintenance_window = ""
      use_metrics            = false
      periods = [
        {
          name        = "mon-9am-start"
          description = "Monday 9AM start"
          begintime   = "09:00"
          endtime     = "23:59"
          weekdays    = ["mon"]
          months      = []
          monthdays   = []
        },
        {
          name        = "tue-thu-full"
          description = "Full days on Tuesday - Thursday"
          begintime   = ""
          endtime     = ""
          weekdays    = ["tue-thu"]
          months      = []
          monthdays   = []
        },
        {
          name        = "fri-6pm-stop"
          description = "Friday 6PM stop"
          begintime   = "00:00"
          endtime     = "18:00"
          weekdays    = ["fri"]
          months      = []
          monthdays   = []
        },
      ]
    },
  }
}
