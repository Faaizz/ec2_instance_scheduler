variable "additional_schedules" {
  description = "Additional schedules."
  type = list(object({
    name                   = string
    description            = string
    timezone               = string
    hibernate              = bool
    enforced               = bool
    use_maintenance_window = bool
    ssm_maintenance_window = string
    use_metrics            = bool
    periods = list(object({
      name        = string
      description = string
      begintime   = string
      endtime     = string
      weekdays    = list(string)
      months      = list(string)
      monthdays   = list(string)
    }))
  }))
  default = []
}

variable "sns_subscriptions" {
  description = "Subscriptions to scheduler's SNS topic."
  type = list(object({
    protocol = string
    endpoint = string
  }))
}
