data "tls_certificate" "gitlab" {
  url = "tls://gitlab.com:443"
}

resource "aws_iam_openid_connect_provider" "gitlab" {
  url = "https://gitlab.com"
  client_id_list = [
    "https://gitlab.com",
  ]
  thumbprint_list = [data.tls_certificate.gitlab.certificates[0].sha1_fingerprint]
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab.arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${aws_iam_openid_connect_provider.gitlab.url}:sub"
      values   = ["project_path:faizudeen.kajogbola/ec2_instance_scheduler:ref_type:branch:ref:main"]
    }
  }

}

resource "aws_iam_role" "gitlab" {
  name_prefix        = "GitLabCI"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AdministratorAccess",
  ]

}
