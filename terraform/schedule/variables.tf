variable "configuration_table" {
  description = "Name of EC2 Instance Scheduler DynamoDB configuration table."
  type        = string
  validation {
    condition = can(regex(
      "[a-zA-Z\\-\\_0-9]+",
      var.configuration_table,
    ))
    error_message = "Please specify a valid DynamoDB table name."
  }
}

variable "name" {
  description = "Schedule name."
  type        = string
  validation {
    condition     = length(var.name) > 0
    error_message = "Please specify a name that is at least 1 character long"
  }
}

variable "description" {
  description = "Schedule description."
  type        = string
  validation {
    condition     = length(var.description) > 0
    error_message = "Please specify a description that is at least 1 character long"
  }
}

variable "timezone" {
  description = "The time zone the schedule will use."
  type        = string
  default     = "UTC"
  validation {
    condition = contains([
      "Africa/Abidjan",
      "Africa/Accra",
      "Africa/Cairo",
      "Africa/Johannesburg",
      "Africa/Lagos",
      "America/New_York",
      "America/Toronto",
      "Europe/Amsterdam",
      "Europe/Berlin",
      "Europe/Dublin",
      "Europe/Istanbul",
      "Europe/London",
      "Europe/Madrid",
      "Europe/Moscow",
      "Europe/Paris",
      "Europe/Rome",
      "Europe/Zurich",
      "GMT",
      "US/Alaska",
      "US/Arizona",
      "US/Central",
      "US/Eastern",
      "US/Hawaii",
      "US/Mountain",
      "US/Pacific",
      "UTC",
    ], var.timezone)
    error_message = "Please specify an allowed time zone."
  }
}

variable "hibernate" {
  description = "Choose whether to hibernate Amazon EC2 instances running Amazon Linux. When this field is set to true, the scheduler will hibernate instances when it stops them. Note that your instances must turn on hibernation and must meet the hibernation prerequisites."
  type        = bool
  default     = false
}

variable "enforced" {
  description = "Choose whether to enforce the schedule. When this field is set to true, the scheduler will stop a running instance if it is manually started outside of the running period or it will start an instance if it is stopped manually during the running period."
  type        = bool
  default     = false
}

variable "use_maintenance_window" {
  description = "Choose whether to add an Amazon RDS maintenance window as a running period to an Amazon RDS instance schedule, or to add an AWS Systems Manager maintenance window as a running period to an Amazon EC2 instance schedule. For more information, refer to Amazon RDS Maintenance Window and SSM Maintenance Window Field."
  type        = bool
  default     = false
}

variable "ssm_maintenance_window" {
  description = "Name of a maintenance window to use."
  type        = string
  default     = ""
}

variable "use_metrics" {
  description = "Turn on CloudWatch metrics at the schedule level. This field overwrites the CloudWatch metrics setting you specified at deployment."
  type        = bool
  default     = false
}

variable "override_status" {
  description = "When this field is set to running, the instance will be started but not stopped until you stop it manually. When this field is set to stopped, the instance will be stopped but not started until you start it manually."
  type        = string
  default     = ""
}

variable "retain_running" {
  description = "Choose whether to prevent the solution from stopping an instance at the end of a running period if the instance was manually started before the beginning of the period."
  type        = bool
  default     = false
}

variable "stop_new_instances" {
  description = "Choose whether to stop an instance the first time it is tagged if it is running outside of the running period. By default, this field is set to true."
  type        = bool
  default     = false
}

variable "periods" {
  description = "Periods for schedule."
  type = list(object({
    name        = string
    description = string
    begintime   = string
    endtime     = string
    weekdays    = list(string)
    months      = list(string)
    monthdays   = list(string)
  }))
}
