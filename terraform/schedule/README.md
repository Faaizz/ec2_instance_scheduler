# schedule

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.55.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.55.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table_item.periods](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/dynamodb_table_item) | resource |
| [aws_dynamodb_table_item.schedule](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/dynamodb_table_item) | resource |
| [aws_dynamodb_table.configuration_table](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/data-sources/dynamodb_table) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_configuration_table"></a> [configuration\_table](#input\_configuration\_table) | Name of EC2 Instance Scheduler DynamoDB configuration table. | `string` | n/a | yes |
| <a name="input_description"></a> [description](#input\_description) | Schedule description. | `string` | n/a | yes |
| <a name="input_enforced"></a> [enforced](#input\_enforced) | Choose whether to enforce the schedule. When this field is set to true, the scheduler will stop a running instance if it is manually started outside of the running period or it will start an instance if it is stopped manually during the running period. | `bool` | `false` | no |
| <a name="input_hibernate"></a> [hibernate](#input\_hibernate) | Choose whether to hibernate Amazon EC2 instances running Amazon Linux. When this field is set to true, the scheduler will hibernate instances when it stops them. Note that your instances must turn on hibernation and must meet the hibernation prerequisites. | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | Schedule name. | `string` | n/a | yes |
| <a name="input_override_status"></a> [override\_status](#input\_override\_status) | When this field is set to running, the instance will be started but not stopped until you stop it manually. When this field is set to stopped, the instance will be stopped but not started until you start it manually. | `string` | `""` | no |
| <a name="input_periods"></a> [periods](#input\_periods) | Periods for schedule. | <pre>list(object({<br>    name        = string<br>    description = string<br>    begintime   = string<br>    endtime     = string<br>    weekdays    = list(string)<br>    months      = list(string)<br>    monthdays   = list(string)<br>  }))</pre> | n/a | yes |
| <a name="input_retain_running"></a> [retain\_running](#input\_retain\_running) | Choose whether to prevent the solution from stopping an instance at the end of a running period if the instance was manually started before the beginning of the period. | `bool` | `false` | no |
| <a name="input_ssm_maintenance_window"></a> [ssm\_maintenance\_window](#input\_ssm\_maintenance\_window) | Name of a maintenance window to use. | `string` | `""` | no |
| <a name="input_stop_new_instances"></a> [stop\_new\_instances](#input\_stop\_new\_instances) | Choose whether to stop an instance the first time it is tagged if it is running outside of the running period. By default, this field is set to true. | `bool` | `false` | no |
| <a name="input_timezone"></a> [timezone](#input\_timezone) | The time zone the schedule will use. | `string` | `"UTC"` | no |
| <a name="input_use_maintenance_window"></a> [use\_maintenance\_window](#input\_use\_maintenance\_window) | Choose whether to add an Amazon RDS maintenance window as a running period to an Amazon RDS instance schedule, or to add an AWS Systems Manager maintenance window as a running period to an Amazon EC2 instance schedule. For more information, refer to Amazon RDS Maintenance Window and SSM Maintenance Window Field. | `bool` | `false` | no |
| <a name="input_use_metrics"></a> [use\_metrics](#input\_use\_metrics) | Turn on CloudWatch metrics at the schedule level. This field overwrites the CloudWatch metrics setting you specified at deployment. | `bool` | `false` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
