locals {
  optional_attributes_schedule = {
    ssm_maintenance_window = var.ssm_maintenance_window
    override_status        = var.override_status
  }

  optional_attributes_period = {
    string_attributes = [
      "description",
      "begintime",
      "endtime",
    ]
    string_set_attributes = [
      "weekdays",
      "months",
      "monthdays",
    ]
  }
}
