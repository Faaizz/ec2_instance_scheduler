resource "aws_dynamodb_table_item" "periods" {
  table_name = data.aws_dynamodb_table.configuration_table.name
  hash_key   = data.aws_dynamodb_table.configuration_table.hash_key
  range_key  = data.aws_dynamodb_table.configuration_table.range_key

  for_each = { for period in var.periods : period.name => period }

  item = jsonencode(merge(
    {
      type = { S = "period" }
      name = { S = "${each.key}-${var.name}" }
    },
    { for str_att in local.optional_attributes_period.string_attributes : str_att => { S = each.value[str_att] } if each.value[str_att] != "" },
    { for str_set_att in local.optional_attributes_period.string_set_attributes : str_set_att => { SS = each.value[str_set_att] } if length(each.value[str_set_att]) > 0 },
  ))
}

resource "aws_dynamodb_table_item" "schedule" {
  table_name = data.aws_dynamodb_table.configuration_table.name
  hash_key   = data.aws_dynamodb_table.configuration_table.hash_key
  range_key  = data.aws_dynamodb_table.configuration_table.range_key

  item = jsonencode(merge(
    {
      type                   = { S = "schedule" }
      name                   = { S = var.name }
      description            = { S = var.description }
      timezone               = { S = var.timezone }
      hibernate              = { BOOL = var.hibernate }
      enforced               = { BOOL = var.enforced }
      use_maintenance_window = { BOOL = var.use_maintenance_window }
      # ssm_maintenance_window = { S = var.ssm_maintenance_window }
      use_metrics = { BOOL = var.use_metrics }
      # override_status = { S = var.override_status }
      retain_running     = { BOOL = var.retain_running }
      stop_new_instances = { BOOL = var.stop_new_instances }
      periods = {
        SS = sort([for _, period in var.periods : "${period.name}-${var.name}"]),
      }
    },
    { for key, val in local.optional_attributes_schedule : key => { S = val } if val != "" },
  ))
}
