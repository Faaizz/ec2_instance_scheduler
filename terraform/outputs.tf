output "cicd_role_arn" {
  description = "Role ARN for CI/CD"
  value       = aws_iam_role.gitlab.arn
}
