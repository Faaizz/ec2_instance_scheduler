additional_schedules = [
  {
    name                   = "office-hours-ng"
    description            = "Office hours Nigeria"
    timezone               = "Africa/Lagos"
    hibernate              = true
    enforced               = false
    use_maintenance_window = false
    ssm_maintenance_window = ""
    use_metrics            = false
    periods = [
      {
        name        = "mon-8am-start"
        description = "Monday 8AM start"
        begintime   = "08:00"
        endtime     = "23:59"
        weekdays    = ["mon"]
        months      = []
        monthdays   = []
      },
      {
        name        = "tue-thu-full"
        description = "Full days on Tuesday - Thursday"
        begintime   = ""
        endtime     = ""
        weekdays    = ["tue-thu"]
        months      = []
        monthdays   = []
      },
      {
        name        = "fri-4pm-stop"
        description = "Friday 4PM stop"
        begintime   = "00:00"
        endtime     = "16:00"
        weekdays    = ["fri"]
        months      = []
        monthdays   = []
      },
    ]
  },
]

sns_subscriptions = [
  {
    protocol = "email"
    endpoint = "faizudeen.kajogbola@cloudreach.com"
  }
]
