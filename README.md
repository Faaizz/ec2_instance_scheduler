# EC2 Instance Scheduler

## Source Path
```
./terraform
```

## Documentation
<!-- BEGIN_TF_DOCS -->
### Requirements

| Name                                                                      | Version |
| ------------------------------------------------------------------------- | ------- |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0   |
| <a name="requirement_aws"></a> [aws](#requirement\_aws)                   | 4.55.0  |
| <a name="requirement_tls"></a> [tls](#requirement\_tls)                   | >= 4.0  |

### Providers

| Name                                              | Version |
| ------------------------------------------------- | ------- |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.55.0  |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 4.0.4   |

### Modules

| Name                                                                                               | Source                | Version |
| -------------------------------------------------------------------------------------------------- | --------------------- | ------- |
| <a name="module_additional_schedules"></a> [additional\_schedules](#module\_additional\_schedules) | ./schedule            | n/a     |
| <a name="module_default_schedules"></a> [default\_schedules](#module\_default\_schedules)          | ./schedule            | n/a     |
| <a name="module_instance_scheduler"></a> [instance\_scheduler](#module\_instance\_scheduler)       | ./instance_scheduler  | n/a     |
| <a name="module_notification_filter"></a> [notification\_filter](#module\_notification\_filter)    | ./notification_filter | n/a     |

### Resources

| Name                                                                                                                                              | Type        |
| ------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_iam_openid_connect_provider.gitlab](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/iam_openid_connect_provider) | resource    |
| [aws_iam_role.gitlab](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/iam_role)                                       | resource    |
| [aws_sns_topic_subscription.stack_creation](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic_subscription)   | resource    |
| [aws_sns_topic_subscription.stack_execution](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic_subscription)  | resource    |
| [aws_iam_policy_document.assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/data-sources/iam_policy_document)  | data source |
| [tls_certificate.gitlab](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/data-sources/certificate)                              | data source |

### Inputs

| Name                                                                                             | Description                             | Type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Default | Required |
| ------------------------------------------------------------------------------------------------ | --------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | :------: |
| <a name="input_additional_schedules"></a> [additional\_schedules](#input\_additional\_schedules) | Additional schedules.                   | <pre>list(object({<br>    name                   = string<br>    description            = string<br>    timezone               = string<br>    hibernate              = bool<br>    enforced               = bool<br>    use_maintenance_window = bool<br>    ssm_maintenance_window = string<br>    use_metrics            = bool<br>    periods = list(object({<br>      name        = string<br>      description = string<br>      begintime   = string<br>      endtime     = string<br>      weekdays    = list(string)<br>      months      = list(string)<br>      monthdays   = list(string)<br>    }))<br>  }))</pre> | `[]`    |    no    |
| <a name="input_sns_subscriptions"></a> [sns\_subscriptions](#input\_sns\_subscriptions)          | Subscriptions to scheduler's SNS topic. | <pre>list(object({<br>    protocol = string<br>    endpoint = string<br>  }))</pre>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | n/a     |   yes    |

### Outputs

| Name                                                                            | Description        |
| ------------------------------------------------------------------------------- | ------------------ |
| <a name="output_cicd_role_arn"></a> [cicd\_role\_arn](#output\_cicd\_role\_arn) | Role ARN for CI/CD |
<!-- END_TF_DOCS --><!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
### Requirements

| Name                                                                      | Version |
| ------------------------------------------------------------------------- | ------- |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0   |
| <a name="requirement_aws"></a> [aws](#requirement\_aws)                   | 4.55.0  |
| <a name="requirement_tls"></a> [tls](#requirement\_tls)                   | >= 4.0  |

### Providers

| Name                                              | Version |
| ------------------------------------------------- | ------- |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.55.0  |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 4.0.4   |

### Modules

| Name                                                                                               | Source                | Version |
| -------------------------------------------------------------------------------------------------- | --------------------- | ------- |
| <a name="module_additional_schedules"></a> [additional\_schedules](#module\_additional\_schedules) | ./schedule            | n/a     |
| <a name="module_default_schedules"></a> [default\_schedules](#module\_default\_schedules)          | ./schedule            | n/a     |
| <a name="module_instance_scheduler"></a> [instance\_scheduler](#module\_instance\_scheduler)       | ./instance_scheduler  | n/a     |
| <a name="module_notification_filter"></a> [notification\_filter](#module\_notification\_filter)    | ./notification_filter | n/a     |

### Resources

| Name                                                                                                                                              | Type        |
| ------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_iam_openid_connect_provider.gitlab](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/iam_openid_connect_provider) | resource    |
| [aws_iam_role.gitlab](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/iam_role)                                       | resource    |
| [aws_sns_topic_subscription.stack_creation](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic_subscription)   | resource    |
| [aws_sns_topic_subscription.stack_execution](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/resources/sns_topic_subscription)  | resource    |
| [aws_iam_policy_document.assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/4.55.0/docs/data-sources/iam_policy_document)  | data source |
| [tls_certificate.gitlab](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/data-sources/certificate)                              | data source |

### Inputs

| Name                                                                                             | Description                             | Type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Default | Required |
| ------------------------------------------------------------------------------------------------ | --------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | :------: |
| <a name="input_additional_schedules"></a> [additional\_schedules](#input\_additional\_schedules) | Additional schedules.                   | <pre>list(object({<br>    name                   = string<br>    description            = string<br>    timezone               = string<br>    hibernate              = bool<br>    enforced               = bool<br>    use_maintenance_window = bool<br>    ssm_maintenance_window = string<br>    use_metrics            = bool<br>    periods = list(object({<br>      name        = string<br>      description = string<br>      begintime   = string<br>      endtime     = string<br>      weekdays    = list(string)<br>      months      = list(string)<br>      monthdays   = list(string)<br>    }))<br>  }))</pre> | `[]`    |    no    |
| <a name="input_sns_subscriptions"></a> [sns\_subscriptions](#input\_sns\_subscriptions)          | Subscriptions to scheduler's SNS topic. | <pre>list(object({<br>    protocol = string<br>    endpoint = string<br>  }))</pre>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | n/a     |   yes    |

### Outputs

| Name                                                                            | Description        |
| ------------------------------------------------------------------------------- | ------------------ |
| <a name="output_cicd_role_arn"></a> [cicd\_role\_arn](#output\_cicd\_role\_arn) | Role ARN for CI/CD |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
